#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente de un UA SIP
"""

import socket
import sys


def main():
    """M´etodo principal"""
    if len(sys.argv) < 3:
        print("Usage: client.py <metodo> <receptor>@<IPreceptor>:<puertosSIP>")
        exit()
    else:

        try:
            method = sys.argv[1]
            sip = sys.argv[2]
            login = sip.split('@')
            user = login[0]
            server = login[1].split(':')
            server_ip, server_port = server
            message = ''

        except (IndexError, ValueError):
            sys.exit("Usage: client_sip.py <metodo> <receptor>@<IPreceptor>:<puertosSIP>")

        if method == 'INVITE':
            message = (method + ' sip:' + user + '@' + server_ip + ' SIP/2.0' + "\r\n")
            sdp = "v=0\r\no=robin@gotham.com 127.0.0.1" + "\r\n"
            sdp += "s=misesion\r\nt=0\r\nm=audio 34543 RTP\r\n"
            cab = "Content-Type: application/sdp\r\n"
            cab += "Content-Length: " + str(len(bytes(sdp, 'utf-8')))+"\r\n\r\n"

            message = message + cab + sdp

        elif method == 'BYE':
            message = (method + ' sip:' + user + '@' + server_ip + ' SIP/2.0')

        else:
            print("Usage: client_sip.py <metodo> <receptor>@<IPreceptor>:<puertosSIP>")
            exit()
        try:

            # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                my_socket.sendto(message.encode('utf-8'), (server_ip, int(server_port)))
                data = my_socket.recv(1024)
                reply = data.decode('utf-8')
                reply = reply.split('\r\n')[2]

                if reply == 'SIP/2.0 200 OK':
                    message = ('ACK sip:' + user + '@' + server_ip + ' SIP/2.0' + "\r\n\r\n")
                    my_socket.sendto(message.encode('utf-8'), (server_ip, int(server_port)))

            print("Cliente terminado.")
        except ConnectionRefusedError:
            print("Error conectando a servidor")


if __name__ == "__main__":
    main()
