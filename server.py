#!/usr/bin/python3
# -*- coding: utf-8 -*-
import random
import socketserver
import sys
import simplertp


class SIPHandler(socketserver.BaseRequestHandler):

    dicc = {}

    def handle(self):

        data = self.request[0]
        sock = self.request[1]
        client_message = data.decode('utf-8')

        if client_message != '\r\n':
            client_elements = ''.join(client_message).split()
            method = client_elements[0]
            others = client_elements[1:]
            sip = others[0].split(':')[0]
            version = others[1]
            reply = b" "

            if sip != 'sip' or version != 'SIP/2.0':
                reply = b"SIP/2.0 400 Bad Request\r\n\r\n"
            else:
                if method == 'INVITE':
                    try:
                        o_sdp = client_message.split('\r\n')[5]
                        usuario_sdp = o_sdp.split('=')[1]
                        ip_sdp = usuario_sdp.split(' ')[1]
                        m_sdp = client_message.split('\r\n')[8]
                        port_sdp = m_sdp.split(' ')[1]
                        self.dicc[ip_sdp] = port_sdp
                        m = "SIP/2.0 100 Trying\r\nSIP/2.0 180 Ringing\r\n"
                        m += "SIP/2.0 200 OK\r\n"
                        sdp = "v=0\r\no=batman@gotham.com 127.0.0.1" + "\r\n"
                        sdp += "s=misesion\r\nt=0\r\nm=audio 67876 RTP\r\n"
                        cab = "Content-Type: application/sdp\r\n"
                        cab += "Content-Length: " + str(len(bytes(sdp, 'utf-8'))) + "\r\n\r\n"

                        reply = bytes(m + cab + sdp, "utf-8") + b"\r\n"

                    except IndexError:
                        reply = b"SIP/2.0 400 Bad Request\r\n"

                elif method == 'BYE':
                    reply = b"SIP/2.0 200 OK\r\n\r\n"

                elif method == 'ACK':
                    RTP_header = simplertp.RtpHeader()
                    ALEAT = random.getrandbits(32)
                    RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0, ssrc=ALEAT)
                    audio_file = sys.argv[3]
                    audio = simplertp.RtpPayloadMp3(audio_file)
                    ip = list(self.dicc)[0]
                    port = int(self.dicc[ip])
                    simplertp.send_rtp_packet(RTP_header, audio, ip, port)

                else:
                    reply = b"SIP/2.0 405 Method Not Allowed\r\n\r\n"

            sock.sendto(reply, self.client_address)


def main():
    try:
        ip = sys.argv[1]
        port = int(sys.argv[2])
        server = socketserver.UDPServer((ip, port), SIPHandler)
    except (IndexError, ValueError):
        sys.exit('Usage: python3 server.py <IP> <port> <audio_file>')

    print("Listening...")
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        print("Servidor finalizado")


if __name__ == "__main__":
    main()
